


export class CreateUserDto {

    email: string;

    fullName: string;

    password: string;

    gender: string;

    created: Date;

    roles: string;

    image: string;
}
