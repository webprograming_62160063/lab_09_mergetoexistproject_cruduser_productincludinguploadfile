import type { Type } from '@/types/Type'
import http from './http'

function addType(Type: Type) {
  return http.post('/Types', Type)
}

function updateType(Type: Type) {
  return http.patch(`/Types/${Type.id}`, Type)
}

function delType(Type: Type) {
  return http.delete(`/Types/${Type.id}`)
}

function getType(id: number) {
  return http.get(`/Types/${id}`)
}

function getTypes() {
  return http.get('/Types')
}

export default { addType, updateType, delType, getType, getTypes }
